'use strict'

var express = require('express');
var multer = require('multer');
var router = express.Router();
const Coffee = require('../models/coffee');
const IngredientList = require('../models/ingredientList');
const User = require('../models/user');
let authToken = 'STARBUCKS';

let storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './public/uploads');
    },
    filename: function (req, file, callback) {
        callback(null, file.fieldname + '-' + Date.now());
    }
});
let uploadComment = multer({storage: storage}).single('comment');
let uploadDrink = multer({storage: storage}).single('drink');

/* GET home page. */
router.get('/', function (req, res, next) {
    Coffee.find(function (err, docs) {
        let coffeeChunks = [];
        let chunkSize = 6;
        for (let i = 0; i < docs.length; i += chunkSize) {
            coffeeChunks.push(docs.slice(i, i + chunkSize));
        }
        res.render('coffee/index', {title: 'Express', coffees: coffeeChunks});
    });
});

router.get('/uploads', function (req, res, next) {
    res.render('../../uploads')
})


router.get('/coffee/detail/:id', function (req, res, next) {
    let name = "";
    let url = "";
    let desc = "";
    let detailList = [];
    let note = "";
    let hashTag = [];

    Coffee.findById(req.params.id, function (err, detail) {
        name = detail.name;
        url = detail.url;
        desc = detail.desc;
        note = detail.note;
        detailList = detail.detail;
        hashTag = detail.hashTag;
        res.render('coffee/detail', {
            name: name,
            url: url,
            details: detailList,
            desc: desc,
            note: note,
            hashTag: hashTag
        });
    });
});

router.get('/api/coffee/detail/:id', function (req, res, next) {
    Coffee.findById(req.params.id, function (err, detail) {
        if (err) return res.send(err);
        return res.send(detail);
    });
});


router.get('/coffee/upload', function (req, res, next) {
    IngredientList.find()
        .sort('name')
        .exec(function (err, docs) {
            let ingredientList = [];
            let chunkSize = 3;
            for (let i = 0; i < docs.length; i += chunkSize) {
                ingredientList.push(docs.slice(i, i + chunkSize));
            }
            res.render('coffee/upload', {title: 'Drink Data', ingredients: ingredientList});
        })
});


router.get('/user/register', function (req, res, next) {
    res.render('user/register');
});

router.get('/user/authenticate', function (req, res, next) {
    res.render('user/authenticate');
});

router.get('/coffee/ingredient', function (req, res, next) {
    IngredientList.find()
        .sort('name')
        .exec(function (err, docs) {
            let ingredientList = [];
            let chunkSize = 3;
            for (let i = 0; i < docs.length; i += chunkSize) {
                ingredientList.push(docs.slice(i, i + chunkSize));
            }
            res.render('coffee/ingredient', {title: 'Ingredient Data', ingredients: ingredientList})
        })
});


//get request for all coffee document
router.get('/api/coffee', function (req, res) {
    Coffee.find(function (err, docs) {
        if (err) return console.log("[Error retrieving documents]");
        if (req.headers.authorization != authToken) {
            return console.log('no token');
        } else
            return res.send(docs);
    })
})

router.get('/api/coffee/search', function (req, res) {
    Coffee.find({$or: [{'name': new RegExp(req.param('search'), 'i')}, {'hashTag': new RegExp(req.param('search'), 'i')}]}, function (err, docs) {
        if (err) return console.log("[Error retrieving documents]");
        if (req.headers.authorization != authToken) {
            return console.log('no token');
        } else
            return res.send(docs);
    })
})

router.get('/api/coffee/category', function (req, res) {
    Coffee.find({'category': new RegExp(req.param('category'), 'i')}, function (err, docs) {
        if (err) return console.log("[Error retrieving documents]");
        if (req.headers.authorization != authToken) {
            return console.log('no token');
        } else
            return res.send(docs);
    })
})

//get request for 10 popular coffee document
router.get('/api/coffee/popular', function (req, res) {
    Coffee
        .find()
        .sort({vote_count: -1})
        .limit(10)
        .exec(function (err, docs) {
            if (err) return res.send(err);
            if (req.headers.authorization != authToken) {
                return res.status(404).send("unauthorized");
            } else
                return res.send(docs);
        })
})

//get request for 10 recent coffee document
router.get('/api/coffee/recent', function (req, res) {
    Coffee
        .find()
        .sort({timeStamp: -1})
        .limit(10)
        .exec(function (err, docs) {
            if (err) return res.send(err);
            if (req.headers.authorization != authToken) {
                return res.status(404).send("unauthorized");
            } else
                return res.send(docs);
        })
})

//display all liked coffee documents
router.get('/api/coffee/voted/:id', function (req, res) {
    Coffee
        .find({vote: req.params.id})
        .exec(function (err, docs) {
            if (err) return res.send(err);
            if (req.headers.authorization != authToken) {
                return res.status(404).send("unauthorized");
            } else
                return res.send(docs);
        })
})


//temporary returns only random
router.get('/api/coffee/featured', function (req, res) {
    if (req.headers.authorization != authToken) {
        return res.status(404).send("unauthorized");
    } else {
        Coffee
            .count()
            .exec(function (err, count) {
                var rdm = Math.floor(Math.random() * count);
                Coffee.findOne().skip(rdm).exec(
                    function (err, result) {
                        return res.send(result);
                    }
                )
            });
    }
});


// =====================================
// PROFILE SECTION =====================
// =====================================
// we will want this protected so you have to be logged in to visit
// we will use route middleware to verify this (the isLoggedIn function)
router.get('/api/profile/:id', function (req, res) {
    User.findById(req.params.id, function (err, user) {
        if (err) return res.send(err);
        return res.send(user);
    });
})

// register local user
// =====================================
// EMAIL ROUTES ========================
// =====================================
router.post('/api/register', function (req, res) {
    let user;
    User.findOne({'local.email': req.body.email}, function (err, result) {

        if (err) {
            res.status(400);
            return res.send(err);
        }
        if (result != null) {
            console.log(result);
            console.log(req.body.email + " exists")
            return res.status(200).send(result);
        } else if (result == null) {
            user = new User();
            user.timeStamp = new Date();
            user.local.email = req.body.email;
            user.local.name = req.body.name;
            user.local.password = user.generateHash(req.body.password);
            user.save(function (err) {
                if (!err) {
                    return res.send(user);
                } else {
                    return res.status(400).send(err);
                }
            })
        }
    })
})

//register facebook user
// =====================================
// FACEBOOK ROUTES =====================
// =====================================
// route for facebook authentication and login

router.post('/api/auth/facebook', function (req, res) {
    let user;
    User.findOne({'facebook.id': req.body.id}, function (err, doc) {
        if (err) {
            res.status(400);
            return res.send(err);
        }
        if (doc == null) {
            user = new User();
            user.timeStamp = new Date();
            user.facebook.id = req.body.id;
            user.facebook.email = req.body.email;
            user.facebook.name = req.body.name;
            user.save(function (err) {
                if (!err) {
                    return res.send(user);
                } else {
                    //set status code to 400 if saving user fails
                    res.status(400)
                    return res.send('error');
                }
            });
        } else if (doc != null) {
            //set status code to 409 if user already exists
            res.status(200);
            res.send(doc);
            return;
        }
    });
});

// =====================================
// GOOGLE ROUTES =======================
// =====================================
// send to google to do the authentication
// profile gets us their basic information including their name
// email gets their emails


// the callback after google has authenticated the user
// router.get('/api/auth/google/callback',
//     passport.authenticate('google', {
//         successRedirect : '../../../successjson',
//         failureRedirect : '../../../failurejson'
//     })
// );

// route for logging out
router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});
//success and fail response
router.get('/successjson', function (req, res) {
    res.send(200);
});
router.get('/failurejson', function (req, res) {
    res.send(401);
});

//post request to insert entry into coffees collection
router.post('/api/coffee', function (req, res) {

    var coffee;
    var hashArray = Array.prototype.slice.call(req.body.hashTag);

    coffee = new Coffee({
        timeStamp: new Date(),
        name: req.body.name,
        url: req.body.url,
        category: req.body.category,
        desc: req.body.desc,
        note: req.body.note,
        detail: req.body.detail,
        hashTag: hashArray
    });

    coffee.save(function (err) {
        if (!err) {
            console.log("[Created item successfully]");
            return res.send(coffee);
        } else {
            //set status code to 400 indicating bad requests
            res.status(400);
            res.send('error');
            console.log(err);
            return;
        }
    });
});

//post request to insert entry into coffees collection
router.post('/api/ingredient', function (req, res) {
    var ingredient;
    ingredient = new IngredientList({
        name: req.body.name,
        calorie: req.body.calorie,
        unit: req.body.unit,
    });
    ingredient.save(function (err) {
        if (!err) {
            console.log("[Created item successfully]");
            return res.send(ingredient);
        } else {
            //set status code to 400 indicating bad requests
            res.status(400);
            res.send('error');
            return;
        }
    });
});

//upload photo
router.post('/api/uploadImage', uploadComment, function (req, res) {
    uploadComment(req, res, function (err) {
        if (err) {
            console.log(err);
            return res.end("Error uploading file.");
        }
        console.log(req.file.filename);
        res.send(req.file.filename);
    });
});

router.post('/api/uploadDrinkImage', uploadDrink, function (req, res) {
    uploadDrink(req, res, function (err) {
        if (err) {
            console.log(err);
            return res.end("Error uploading file.");
        }
        res.send(req.file.filename);
    });
});

//device id vote for the coffee
router.put('/api/coffee/vote/:id', function (req, res) {
    console.log("REQUEST VOTE: ");
    Coffee.findById(req.params.id, function (err, drink) {
        if (err) return res.send(err);
        if (drink.vote.indexOf(req.body.vote) > -1) {
            res.status(400);
            res.send("Duplicate");
            console.log("Duplicate");
            return;
        }
        console.log("Success");
        drink.vote.addToSet(req.body.vote);
        drink.vote_count = drink.vote.length;
        drink.save(function (err) {
            if (err) return res.send(err);
            return res.json(drink);
        })
    });
});

router.put('/api/coffee/comment/:id', function (req, res) {
    console.log("REQUEST COMMENT: ");
    console.log(req.body);
    Coffee.findById(req.params.id, function (err, drink) {
        if (err) return res.send(err);
        console.log("Success");
        drink.comment.push({
            'timeStamp': new Date().toUTCString(),
            'userId': req.body.userId,
            'userName': req.body.userName,
            'comment': req.body.comment,
            'url': req.body.url
        });
        drink.save(function (err) {
            if (err) return res.send(err);
            return res.json(drink);
        })
    });
});

//post request to insert entry into coffees collection
router.delete('/api/ingredient', function (req, res) {

    IngredientList.remove({'name': req.param('item')}).exec(function (err, docs) {
        return res.send(docs);
    })


});

module.exports = router;