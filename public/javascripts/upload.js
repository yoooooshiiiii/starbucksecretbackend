
/**
 * Created by dkim on 11/1/2016.
 */
var url;
var ingredient;
let array2 = [];
let array3 = [];
$(function() {
    $('#submit').click(function(e) {
        upload();
        return false;
    });
    $('#add').click(function(e) {
        add();
        return false;
    });
    $('#file').change(function(){
        data = new FormData($('#picForm'));

        $.ajax({
            type: "POST",
            url: "/api/uploadDrinkImage",
            data: new FormData($("#picForm")[0]),
            contentType: false,
            processData: false,
            success: (function(data){
                if (data != null) {
                    testImage("../public/uploads/" + data);
                    url = "../public/uploads/" + data;
                }
            }),
            //on status code other than 200, invoke function
            error: (function(data){
                console.log(data);
                alert('Invalid entry');
            })
        })
    });
});

// function submitForms(){
//     document.getElementById("uploadForm").submit();
//     upload();
//     return false;
// }

function setImagePath() {
    urlBox = $('#url').val();
}

function testImage(url) {
    var img = document.createElement("IMG");
    img.src = url;
    $('#image').attr('src', url);
}

function add(){
    let name = $('option:selected').attr('name');
    let calorie =  $('option:selected').attr('calorie');
    let unit =  $('option:selected').attr('unit');
    let array1 = [];
    let ingredient = {};
    array1.push(name, calorie, unit);
    ingredient["item"]= array1[0];
    ingredient["serving"] = $('#serving').val();
    ingredient["calorie"] = array1[1] * $('#serving').val();
    ingredient["unit"]= array1[2];
    array3.push({ingredient:ingredient});
    document.getElementById("ingList").innerHTML += ingredient["item"] + "<br>" + ingredient["serving"] + " "+ unit +"<br>";
}


function upload() {
    var hashTag = $('#hashTag').val();
    var hashArray = hashTag.split(" ");
    $.ajax({
        type: "POST",
        url: "/api/coffee",
        dataType: "json",
        data: JSON.stringify({
            "name": $('#name').val(),
            "category": $('input[name=category]:checked').val(),
            "url": url,
            "desc": $('#desc').val(),
            "note": $('#note').val(),
            "detail": array3,
            "hashTag": hashArray
        }),
        contentType: "application/json; charset=UTF-8",
        success: (function(data){
            if (data != null) {
                console.log('Successful');
                window.location.href = '../../';
            }
        }),
        //on status code other than 200, invoke function
        error: (function(data){
            console.log(data);
            alert('Invalid entry');
        })
    })
}