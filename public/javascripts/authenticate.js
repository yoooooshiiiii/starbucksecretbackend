/**
 * Created by dkim on 12/7/2016.
 */

$(function() {
    $('#submit').click(function(e) {
        authenticate();
        return false;
    });
});

function authenticate() {
    $.ajax({
        type: "GET",
        url: "/api/authenticate",
        dataType: "json",
        data: { password: $('#password').val()},
        contentType: "application/json; charset=UTF-8",
        success: (function(data){
        }),
        //on status code other than 200, invoke function
        error: (function(data){
        })
    })
}