/**
 * Created by dkim on 6/6/2017.
 */
/**
 * Created by dkim on 11/1/2016.
 */

$(function() {
    $('#submit').click(function(e) {
        upload();
        return false;
    });
    $("button").click(function(e) {
        deleteItem($(this).val());
        return true;
    })
});

function deleteItem(deleteItem) {
    $.ajax({
        type: "DELETE",
        url: "/api/ingredient",
        dataType: "json",
        data: JSON.stringify({ "item" : deleteItem }),
        contentType: "application/json; charset=UTF-8",
        success: (function(data){
            if (data != null) {
                console.log('Successful');
                window.location.href = '../coffee/ingredient';
            }
        }),
        //on status code other than 200, invoke function
        error: (function(data){
            alert('Invalid entry');
        })
    })
}

function upload() {
        $.ajax({
            type: "POST",
            url: "/api/ingredient",
            dataType: "json",
            data: JSON.stringify({
                "name": $('#name').val(),
                "calorie": $('#calorie').val(),
                "unit": $('#unit').val()
            }),
            contentType: "application/json; charset=UTF-8",
            success: (function (data) {
                if (data != null) {
                    console.log('Successful');
                    window.location.href = '../coffee/ingredient';
                }
            }),
            //on status code other than 200, invoke function
            error: (function (data) {
                alert('Invalid entry');
            })
        })
}