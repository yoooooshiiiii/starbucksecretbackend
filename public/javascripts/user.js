/**
 * Created by dkim on 12/7/2016.
 */

$(function() {
    $('#submit').click(function(e) {
        register();
        return false;
    });
});

function register() {
    $.ajax({
        type: "POST",
        url: "/api/register",
        dataType: "json",
        data: JSON.stringify({
            "name": $('#name').val(),
            "email": $('#email').val(),
            "password": $('#password').val()
        }),
        contentType: "application/json; charset=UTF-8",
        success: (function(data){
            if (data != null) {
                console.log('Successful');
                window.location.href = '../../';
            }
        }),
        //on status code other than 200, invoke function
        error: (function(data){
            alert('Invalid entry');
        })
    })
}