/**
 * Created by dkim on 6/6/2017.
 */
'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ingredientList = new Schema({
    name : {type: String, unique: true},
    calorie : Number,
    unit : String,
});

module.exports = mongoose.model('IngredientList', ingredientList);