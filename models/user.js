/**
 * Created by dkim on 12/6/2016.
 */
'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const user = new Schema({
    timeStamp   : Date,
    local    :  {
        email : String,
        password : String,
        status      : {type: String, default: "User"}
    }
});


module.exports = mongoose.model('User', user);