'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const User = require('../models/user');

const coffee = new Schema({
    timeStamp       :   Date,
    name            :   {type: String, unique: true},
    url             :   String,
    category        :   String,
    desc            :   String,
    note            :   String,
    detail          : [{
        ingredient  : {
                                item: String,
                                serving: Number,
                                unit: String,
                                calorie: Number },
    }],
    comment         : [{
        timeStamp   :   Date,
        userId      :   String,
        userName    :   String,
        comment     :   String,
        url         :   String
    }],
    vote            :   [{ type: String, unique: true }],
    vote_count      :   { type: Number, default: 0 },
    hashTag         :   [{ type: String, index: true }],
    user            :   [{type: Schema.Types.ObjectId, ref: 'User'}]
});

module.exports = mongoose.model('Coffee', coffee);